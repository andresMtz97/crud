<?php

namespace App\Controller\Component;

use Cake\Controller\Component;

class PasswordComponent extends Component
{
    function generatePassword ($length = 8)
    {
        // inicializa variables
        $password = "";
        $i = 0;
        $possible = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdfghijklmnopqrstvwxyz.";

        // agrega random
        while ($i < $length) {
            $char = substr($possible, mt_rand(0, strlen($possible)-1), 1);

            if (!strstr($password, $char)) {
                $password .= $char;
                $i++;
            }
        }
        return $password;
    }
}
