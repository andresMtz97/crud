<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Mailer\MailerAwareTrait;
use Cake\ORM\TableRegistry;
use Cake\I18n\Time;
use Cake\Routing\Router;
use Cake\Mailer\Email;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 *
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class UsersController extends AppController
{
    public $paginate = [
        'limit' => 10,
        'order' => [
            'Users.a_paterno' => 'asc'
        ]
    ];

    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('Password');

        // $this->Auth->allow(['logout', 'reset-password']);

        $this->loadComponent('Recaptcha.Recaptcha', [
            'enable' => true,     // true/false
            'sitekey' => '6Lc-24sUAAAAAFdNibAyOSA-VsrnJQDv9M1tS6gr',
            'secret' => '6Lc-24sUAAAAAO3lfhU3sGpYcwegLrMp2jPvOyUf',
            'type' => 'image',  // image/audio
            'theme' => 'light', // light/dark
            'lang' => 'es',      // default en
            'size' => 'normal'  // normal/compact
        ]);

        // $this->loadHelper('Form', ['errorClass' => 'form-control col-sm-6 is-invalid']);
    }

    public function beforeFilter(\Cake\Event\Event $event){
        parent::beforeFilter($event);
        $this->Auth->allow(['resetPasswordRequest', 'resetPassword']);
    }

    public function isAuthorized($user){
        if (isset($user['administrador']) && !$user['administrador']) {
            if (in_array($this->request->action, ['index', 'logout', 'changePassword'])) {
                return true;
            }
        }
        return parent::isAuthorized($user);
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $users = $this->paginate($this->Users);
        $login = $this->Auth->user();
        $this->set(compact('users'));
        $this->set('login', $login);
    }

    /**
     * View method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => []
        ]);

        $this->set('user', $user);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
     use MailerAwareTrait;
    public function add()
    {
        $user = $this->Users->newEntity();
        $password = $this->Password->generatePassword();
        if ($this->request->is('post')) {
            $data = $this->request->getData();
            $password = $data['password'];
            $user = $this->Users->patchEntity($user, $data,['fields' => ['email', 'password', 'nombre', 'a_paterno', 'a_materno', 'habilitado', 'administrador', 'photo']]);
            if(!$user->errors()){
                if ($this->Users->save($user)) {
                    $this->getMailer('Users')->send('sendPassword', [$user, $password]);
                    TableRegistry::get('Bitacora')->saveRegistry($this->Auth->user('email'), "REGISTRO USUARIO");
                    $this->Flash->success(__('El usuario ha sido agregado.'));
                    return $this->redirect(['action' => 'index']);
                }
                $this->Flash->error(__('El usuario no puede ser agregado. Por favor, intente de nuevo.'));
            }            
        }
        $this->set(compact('user'));
        $this->set('password', $password);
    }

    /**
     * Edit method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $user = $this->Users->patchEntity($user, $this->request->getData(), ['fields' => ['nombre', 'a_paterno', 'a_materno', 'habilitado', 'administrador', 'photo']]);
            if(!$user->errors()){
                if ($this->Users->save($user)) {
                TableRegistry::get('Bitacora')->saveRegistry($this->Auth->user('email'), "MODIFICACION USUARIO");
                $this->Flash->success(__('El usuario ha sido guardado.'));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('El usuario no puede ser agregado. Por favor, intente de nuevo.'));
            }
        }
        $this->set(compact('user'));
    }

    /**
     * Delete method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $user = $this->Users->get($id);
        if ($this->Users->delete($user)) {
            TableRegistry::get('Bitacora')->saveRegistry($this->Auth->user('email'), "ELIMINACION USUARIO");
            $this->Flash->success(__('El usuario ha sido eliminado.'));
        } else {
            $this->Flash->error(__('El usuario no pudo ser eliminado. Por favor, intente de nuevo.'));
        }

        return $this->redirect(['action' => 'index']);
    }




    public function login()
    {
        if($this->Auth->user()){
            return $this->redirect($this->Auth->redirectUrl()); 
        }
        if ($this->request->is('post')) {
            if ($this->Recaptcha->verify()) {
                $user = $this->Auth->identify();
                if ($user) {
                    $this->Auth->setUser($user);
                    TableRegistry::get('Bitacora')->saveRegistry($this->Auth->user('email'), "LOGIN");
                    return $this->redirect($this->Auth->redirectUrl());
                }
                $this->Flash->error('El nombre de usuario o contraseña es incorrecto.', ['key' => 'auth']);
                return $this->redirect($this->referer());
            }
            $this->Flash->error(__('Por favor verifique Google Recaptcha primero'));
        }
        
    }

    public function logout()
    {
        // $this->Flash->success('Haz cerrado sesión.');
        TableRegistry::get('Bitacora')->saveRegistry($this->Auth->user('email'), "LOGOUT");
        return $this->redirect($this->Auth->logout());
    }

    /**
     * Descripcion
     *
     * @params
     */
    public function changePassword($id = null){
        $user = $this->Users->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $user = $this->Users->patchEntity($user, $this->request->getData(), ['validate' => 'changePassword']);
            $data = $this->request->data;
            if(password_verify($data['oldPassword'], $user['password'])){
                if ($data['newPassword'] === $data['newPassword2']) {
                    $user['password'] = $data['newPassword'];
                    if(!$user->errors()){
                        if ($this->Users->save($user)) {
                            TableRegistry::get('Bitacora')->saveRegistry($this->Auth->user('email'), "CAMBIO CONTRASEÑA");
                            $this->Flash->success(__('La contraseña ha sido modificada.'));
                            return $this->redirect(['action' => 'index']);
                        }
                        $this->Flash->error(__('No se puede cambiar la contraseña. Vuelva a intentar'));
                    }
                }
                else {
                    $this->Flash->error(__('La contraseña nueva no coincide.'));
                }
            }
            else {
                $this->Flash->error(__('La contraseña actual es incorrecta.'));
            }
        }
        $this->set(compact('user'));
    }

    public function resetPasswordRequest()
    {
        if($this->Auth->user()){
            return $this->redirect($this->Auth->redirectUrl()); 
        }
        if ($this->request->is('post')) {
            $query = $this->Users->findByEmail($this->request->data['email']);
            $user = $query->first();
            if (is_null($user)) {
                $this->Flash->error('El Email no está registrado. Por favor, intente de nuevo.');
            } else {
                $passkey = uniqid();
                $url = Router::Url(['controller' => 'users', 'action' => 'resetPassword'], true) . '/' . $passkey;
                $timeout = time() + DAY;
                 if ($this->Users->updateAll(['passkey' => $passkey, 'timeout' => $timeout], ['id' => $user->id])){
                     if ($this->getMailer('Users')->send('sendResetEmail', [$url, $user])) {
                         $this->Flash->success(__('Revisa tu correo, el link para restablecer la contraseña ha sido enviado.'));
                     } else {
                         $this->Flash->error(__('Error enviando a la dirección de correo: ') . $email->smtpError);
                     }
                    $this->redirect(['action' => 'login']);
                } else {
                    $this->Flash->error('Error al salvar passkey/tiempo');
                }
            }
        }
    }

    public function resetPassword($passkey = null) {
        if ($passkey) {
            $query = $this->Users->find('all', ['conditions' => ['passkey' => $passkey, 'timeout >' => time()]]);
            $user = $query->first();
            if ($user) {
                if (!empty($this->request->data)) {
                    if ($this->request->data['password'] === $this->request->data['confirm_password']) {
                        $user = $this->Users->patchEntity($user, $this->request->data, ['validate' => 'resetPassword']);
                        $user->passkey = null;
                        $user->timeout = null;
                        if(!$user->errors()){
                            if ($this->Users->save($user)) {
                                TableRegistry::get('Bitacora')->saveRegistry($user->email, "RESET PASSWORD");
                                $this->Flash->success(__('Tu contraseña ha sido actualizada.'));
                                return $this->redirect(array('action' => 'login'));
                            }
                            $this->Flash->error(__('La contraseña no puede ser actualizada. Por favor, intente de nuevo.'));
                        }
                    }
                    else {
                        $this->Flash->error(__('Las contraseñas no coinciden. Vuelva a intentar'));
                    }
                }
            } else {
                $this->Flash->error('Link inválido o expirado. Por favor, revisa tu correo o intenta de nuevo.');
                $this->redirect(['action' => 'login']);
            }
            unset($user->password);
            $this->set(compact('user'));
        } else {
            $this->redirect('/');
        }
    }
}
