<?php
$this->assign('title', 'Reset Password');
$this->loadHelper('Form', [
   'errorClass' => 'form-control col-sm-6 is-invalid',
]);
?>
<div class="container">
    <?php echo $this->Form->create($user) ?>
    <fieldset>
        <legend style="text-align: center;"><?php echo __('Restablecer contraseña') ?>
        <hr>
    <?php
        echo $this->Form->input('password', ['required' => true,
            'autofocus' => true,
            'label' => 'Contraseña',
            'templates' => [
                'error' => '<div class="invalid-feedback offset-sm-4 col-sm-6 {{type}}">{{content}}</div>',
                'input' => '<input type="{{type}}" name="{{name}}"{{attrs}} class="form-control col-sm-6"/>',
                'inputContainer' => '<div class="form-group row {{type}}{{required}}">{{content}}</div>',
                'inputContainerError' => '<div class="form-group row">{{content}}{{error}}</div>',
                'label' => '<label{{attrs}} class="col-sm-2 col-form-label offset-sm-2">{{text}}</label>',
            ]
        ]); ?>

    <?php
        echo $this->Form->input('confirm_password', ['type' => 'password',
            'required' => true,
            'label' => 'Confirma contraseña',
            'templates' => [
                'error' => '<div class="invalid-feedback offset-sm-4 col-sm-6 {{type}}">{{content}}</div>',
                'input' => '<input type="{{type}}" name="{{name}}"{{attrs}} class="form-control col-sm-6"/>',
                'inputContainer' => '<div class="form-group row {{type}}{{required}}">{{content}}</div>',
                'inputContainerError' => '<div class="form-group row">{{content}}{{error}}</div>',
                'label' => '<label{{attrs}} class="col-sm-2 col-form-label offset-sm-2">{{text}}</label>',
            ]
        ]);
    ?>
    </fieldset>
    <div class="form-group">
        <?php echo $this->Form->button(__('Submit'), ['class' => 'btn btn-primary btn-block']); ?>
    </div>
    <?php echo $this->Form->end(); ?>
</div>
