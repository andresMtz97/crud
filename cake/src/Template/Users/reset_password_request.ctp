<?php $this->assign('title', 'Request Password Reset'); ?><div class="users content container" style="text-align: center">
	<h3><?php echo __('Olvidó la contraseña'); ?></h3>
    <hr>
    	<?= $this->Form->create(); ?>
        <?= $this->Form->input('email', ['autofocus' => true,
                                        'label' => 'Email',
                                        'required' => true,
                                        'templates' => [
                                            'inputContainer' => '<div class="form-group row {{type}}{{required}}">{{content}}</div>',
                                            'label' => '<label{{attrs}} class="col-sm-2 col-form-label offset-sm-2">{{text}}</label>',
                                            'input' => '<input type="{{type}}" name="{{name}}"{{attrs}} class="form-control col-sm-6"/>',
                                            'error' => '<div class="error-message invalid-feedback">{{content}}</div>',
                                            'inputContainerError' => '<div class="input form-group {{type}}{{required}} error">{{content}}{{error}}</div>',
                                        ]]); ?>
        <div class="form-group row">
            <?= $this->Form->button('Enviar link', ['class' => 'btn btn-block btn-primary col-sm-6 offset-sm-4']); ?>
        </div>
    	<?= $this->Form->end(); ?>

</div>
