<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User[]|\Cake\Collection\CollectionInterface $users
 */
?>
<nav class="navbar navbar-expand-lg navbar-dark bg-primary fixed-top">
        <?= $this->Html->link(__('Usuarios'), ['action' => 'index'], ['class' => 'navbar-brand h1 mb-0']) ?>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarToggler" aria-controls="navbarToggler" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarToggler">
        <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
            <li class="nav-item active">
                <?php if($login['administrador']) {
                    echo $this->Html->link(__('Nuevo Usuario'), ['action' => 'add'], ['class' => 'nav-link']);
                } ?>
            </li>

        </ul>
        <ul class="navbar-nav ml-auto mt-2 mt-lg-0">
            <li class="nav-item active">
                <?= $this->Html->link(__('Cambiar Contraseña'), ['action' => 'changePassword', $login['id']], ['class' => 'nav-link']); ?>
            </li>
            <li class="nav-item active my-2 my-sm-0">
                <?= $this->Html->link(__('Cerrar Sesión'), ['action' => 'logout'], ['class' => 'nav-link btn btn-danger btn-sm ']) ?>
            </li>
        </ul>
    </div>
</nav>

<div class="container-fluid">
    <table class="table table-bordered table-sm table-hover" cellpadding="0" cellspacing="0">
        <thead class="thead-light">
            <tr>
                <th scope="col"><?= $this->Paginator->sort('a_paterno', 'Apellido Paterno') ?></th>
                <th scope="col"><?= $this->Paginator->sort('a_materno', 'Apellido Materno') ?></th>
                <th scope="col"><?= $this->Paginator->sort('nombre', 'Nombre') ?></th>
                <th scope="col"><?= $this->Paginator->sort('email', 'Email') ?></th>
                <th scope="col"><?= __('Estado') ?></th>
                <th scope="col"><?= __('Tipo') ?></th>
                <?php if($login['administrador']) { ?>
                    <th scope="col" class="actions"><?= __('') ?></th>
                <?php } ?>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($users as $user): ?>
            <tr>
                <td><?= h($user->a_paterno) ?></td>
                <td><?= h($user->a_materno) ?></td>
                <td><?= h($user->nombre) ?></td>
                <td><?= h($user->email) ?></td>
                <td><?= h($user->habilitado ? __('Habilitado') : __('Deshabilitado')) ?></td>
                <td><?= h($user->administrador ? __('Administrador') : __('Usuario')) ?></td>
                <?php if($login['administrador']) { ?>
                    <td class="actions">
                        <!-- <?= $this->Html->link(__('Ver'), ['action' => 'view', $user->id], ['class' => 'btn btn-sm btn-primary']) ?> -->
                        <?= $this->Html->link(__('Editar'), ['action' => 'edit', $user->id], ['class' => 'btn btn-sm btn-info col-lg-5']) ?>
                        <?= $this->Form->postLink(__('Borrar'), ['action' => 'delete', $user->id],
                            ['confirm' => __('Estás seguro que quieres borrar a {0}?', $user->email),
                            'class' => 'btn btn-sm btn-danger  col-lg-5']) ?>
                    </td>
                <?php } ?>

            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <nav aria-label="Page navigation example">
        <?php
           $this->Paginator->templates([
               'prevActive' => '<li class="page-item"><a class="page-link" href="{{url}}">{{text}}</a></li>',
               'prevDisabled' => '<li class="page-item disabled"><a class="page-link" href="{{url}}">{{text}}</a></li>',
               'number' => '<li class="page-item"><a class="page-link" href="{{url}}">{{text}}</a></li>',
               'current' => '<li class="page-item"><a class="page-link" href="{{url}}">{{text}}</a></li>',
               'nextActive' => '<li class="page-item"><a class="page-link" href="{{url}}">{{text}}</a></li>',
               'nextDisabled' => '<li class="page-item disabled"><a class="page-link" href="{{url}}">{{text}}</a></li>'
           ]); ?>
        <ul class="pagination justify-content-center">
            <?= $this->Paginator->prev(__('Anterior')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('Siguiente')) ?>
        </ul>
    </nav>

    <p><?= $this->Paginator->counter(['format' => __('Página {{page}} de {{pages}}, mostrando {{current}} registro(s) de {{count}} total')]) ?></p>

</div>
<!-- <?php
 print_r($_SESSION['Auth']); ?> -->
