<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 */
 $this->loadHelper('Form', [
    'errorClass' => 'form-control col-sm-6 is-invalid',
]);
?>
 <?= $this->Html->script('photo.js') ?>

<nav class="navbar navbar-light navbar-expand-lg bg-primary fixed-top">
    <?= $this->Html->link(__('Usuarios'), ['action' => 'index'], ['class' => 'navbar-brand']) ?>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarText">
        <ul class="navbar-nav ml-auto">
            <li class="nav-item active col-sm-7"><?= $this->Html->link(__('Lista Usuarios'), ['action' => 'index'],['class' => 'nav-link btn btn-outline-light btn-sm']) ?></li>
            <li class="nav-item active col-sm-5"><?= $this->Form->postLink(
                    __('Delete'),
                    ['action' => 'delete', $user->id],
                    ['confirm' => __('Estás seguro que quieres borrar a {0}?', $user->email),
                    'class' => 'nav-link btn btn-outline-danger btn-sm']
                )
            ?></li>
        </ul>
    </div>

</nav>

<div class="" style="text-align: center">
    <legend><?= __('Editar Usuario') ?></legend>
</div>
<div class="row">
    <div class="col-sm-12 col-lg-2" style="align: center">
        <?= $this->Html->image('/webroot/files/users/photo/' . $user->photo_dir . '/' . $user->photo, ['style' => 'height: 200px; width: auto;']); ?>
    </div>

    <div class=" col-lg-10">
        <?= $this->Form->create($user, ['novalidate', 'type' => 'file']) ?>
        <fieldset>


            <?= $this->Form->control('email', [
                'disabled' => 'true',
                'templates' => [
                    'error' => '<div class="invalid-feedback offset-sm-2 {{type}}">{{content}}</div>',
                    'input' => '<input type="{{type}}" name="{{name}}"{{attrs}} class="form-control col-sm-6"/>',
                    'inputContainer' => '<div class="form-group row {{type}}{{required}}">{{content}}</div>',
                    'inputContainerError' => '<div class="form-group row">{{content}}{{error}}</div>',
                    'label' => '<label{{attrs}} class="col-sm-2 col-form-label">{{text}}</label>',
                ],
            ]) ?>

            <?= $this->Form->control('nombre', [
                'templates' => [
                    'error' => '<div class="invalid-feedback offset-sm-2">{{content}}</div>',
                    'input' => '<input type="{{type}}" name="{{name}}"{{attrs}} class="form-control col-sm-6"/>',
                    'inputContainer' => '<div class="form-group row {{type}}{{required}}">{{content}}</div>',
                    'inputContainerError' => '<div class="form-group row {{type}}{{required}} error ">{{content}}{{error}}</div>',
                    'label' => '<label{{attrs}} class="col-sm-2 col-form-label">Nombre(s)</label>',
                ]]) ?>

            <?= $this->Form->control('a_paterno', [
                'templates' => [
                    'error' => '<div class="invalid-feedback offset-sm-2">{{content}}</div>',
                    'input' => '<input type="{{type}}" name="{{name}}"{{attrs}} class="form-control col-sm-6"/>',
                    'inputContainer' => '<div class="form-group row {{type}}{{required}}">{{content}}</div>',
                    'inputContainerError' => '<div class="form-group row {{type}}{{required}} error ">{{content}}{{error}}</div>',
                    'label' => '<label{{attrs}} class="col-sm-2 col-form-label">Apellido Paterno</label>',
                ]]) ?>

            <?= $this->Form->control('a_materno', [
                'templates' => [
                    'error' => '<div class="invalid-feedback offset-sm-2">{{content}}</div>',
                    'input' => '<input type="{{type}}" name="{{name}}"{{attrs}} class="form-control col-sm-6"/>',
                    'inputContainer' => '<div class="form-group row {{type}}{{required}}">{{content}}</div>',
                    'inputContainerError' => '<div class="form-group row {{type}}{{required}} error">{{content}}{{error}}</div>',
                    'label' => '<label{{attrs}} class="col-sm-2 col-form-label">Apellido Materno</label>',
                ]]) ?>

            <div class="form-check form-group row">
                <div class="col-sm-6 offset-sm-2 custom-control custom-checkbox">
                    <?= $this->Form->checkbox('habilitado', ['class' => 'custom-control-input', 'id' => 'habilitado']); ?>
                    <?= $this->Form->label('habilitado', 'Habilitado', ['class' => 'custom-control-label'] ); ?>
                </div>
            </div>

            <div class="form-check form-group row">
                <div class="col-sm-6 offset-sm-2 custom-control custom-checkbox">
                    <?= $this->Form->checkbox('administrador', ['class' => 'custom-control-input', 'id' => 'administrador']); ?>
                    <?= $this->Form->label('administrador', 'Administrador', ['class' => 'custom-control-label'] ); ?>
                </div>
            </div>

            <?= $this->Form->input('photo', [
                'type' => 'file',
                'class' => 'custom-file-input ',
                'templates' => [
                    'error' => '<div class="invalid-feedback offset-sm-3">{{content}}</div>',
                    'inputContainer' => '<div class="custom-file col-sm-8 row {{type}}{{required}}">{{content}}</div>',
                    'inputContainerError' => '<div class="custom-file  col-sm-8 {{type}}{{required}} error">{{content}}{{error}}</div>',
                    'label' => '<label{{attrs}} class="custom-file-label offset-sm-3">Foto de usuario</label>',
                ]
            ]); ?>
        </fieldset>

        <div class="row" style="margin-top: 20px">
            <div class="btn-group btn-group-lg offset-sm-3 col-sm-2">
                <?= $this->Form->button(__('Enviar'), ['class' => 'btn btn-primary btn-block']) ?>
            </div>
            <div class="btn-group btn-group-lg col-sm-2">
                <?= $this->Html->link(__('Cancelar'), ['action' => 'index'],['class' => 'btn btn-danger btn-block']) ?>
            </div>
        </div>

        <?= $this->Form->end() ?>
    </div>
</div>
