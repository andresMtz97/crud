<div class="" style="text-align: center">
    <legend><h1>Login</h1></legend>
</div>
<hr>

<div class="container">
    <?= $this->Flash->render('auth') ?>
    <?= $this->Form->create(null, ['class' => 'was-validated']) ?>
    <fieldset>
        <legend style="text-align: center"><?= __('Por favor ingrese su email y contraseña') ?></legend>
        <?= $this->Form->control('email', [
            'templates' => [
                'inputContainer' => '<div class="form-group {{type}}{{required}}">{{content}}</div>',
                'label' => '<label{{attrs}} class="col-sm-2 offset-sm-4 col-form-label" hidden>{{text}}</label>',
                'input' => '<input type="{{type}}" name="{{name}}"{{attrs}} class="form-control col-sm-4 offset-sm-4" placeholder="Email" required/>',
                'error' => '<div class="error-message invalid-feedback">{{content}}</div>',
                'inputContainerError' => '<div class="input {{type}}{{required}} error invalid-feedback">{{content}}{{error}}</div>',
            ]]) ?>
        <?= $this->Form->control('password', [
            'templates' => [
                'inputContainer' => '<div class="form-group {{type}}{{required}}">{{content}}</div>',
                'label' => '<label{{attrs}} class="col-sm-2 offset-sm-4 col-form-label" hidden>Contraseña</label>',
                'input' => '<input type="{{type}}" name="{{name}}"{{attrs}} class="form-control col-sm-4 offset-sm-4" placeholder="Contraseña" required/>',
                'error' => '<div class="error-message invalid-feedback">{{content}}</div>',
                'inputContainerError' => '<div class="input {{type}}{{required}} error invalid-feedback">{{content}}{{error}}</div>',
            ]]) ?>
    </fieldset>

    <div class="offset-sm-4 col-sm-4" style="padding-left: 35px">
        <?= $this->Recaptcha->display() ?>
    </div>

    <br>

    <?= $this->Form->button('Entrar', ['class' => 'btn btn-info btn-block col-sm-4 offset-sm-4']) ?>

    <?= $this->Form->end() ?>

    <?= $this->Html->link(__('¿Olvidó la contraseña?'), ['action' => 'resetPasswordRequest']) ?>
</div>
