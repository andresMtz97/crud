<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 */
 $this->loadHelper('Form', [
    'errorClass' => 'form-control col-sm-6 is-invalid',
]);
?>

 <?= $this->Html->script('photo.js') ?>

<nav class="navbar navbar-dark bg-primary fixed-top">
    <?= $this->Html->link(__('Usuarios'), ['action' => 'index'], ['class' => 'navbar-brand']) ?>
</nav>
<div class="container">
    <?= $this->Form->create($user, ['type' => 'file', 'novalidate']) ?>

    <fieldset>
        <div class="" style="text-align: center">
            <legend><?= __('Nuevo Usuario') ?></legend>
        </div>

        <hr style="border-top:1px solid black;">

        <?= $this->Form->control('email', [
            'templates' => [
                'error' => '<div class="invalid-feedback offset-sm-4 {{type}}">{{content}}</div>',
                'input' => '<input type="{{type}}" name="{{name}}"{{attrs}} class="form-control col-sm-6" />',
                'inputContainer' => '<div class="form-group row {{type}}{{required}}">{{content}}</div>',
                'inputContainerError' => '<div class="form-group row">{{content}}{{error}}</div>',
                'label' => '<label{{attrs}} class="col-sm-2 offset-sm-2 col-form-label">{{text}}</label>',
            ],
        ]) ?>

        <?= $this->Form->password('password', ['type' => 'hidden', 'value' => $password]);  ?>

        <?= $this->Form->control('nombre', [
            'templates' => [
                'error' => '<div class="invalid-feedback offset-sm-4">{{content}}</div>',
                'input' => '<input type="{{type}}" name="{{name}}"{{attrs}} class="form-control col-sm-6"/>',
                'inputContainer' => '<div class="form-group row {{type}}{{required}}">{{content}}</div>',
                'inputContainerError' => '<div class="form-group row {{type}}{{required}} error">{{content}}{{error}}</div>',
                'label' => '<label{{attrs}} class="col-sm-2 offset-sm-2 col-form-label">Nombre(s)</label>',
            ]]) ?>

        <?= $this->Form->control('a_paterno', [
            'templates' => [
                'error' => '<div class="invalid-feedback offset-sm-4">{{content}}</div>',
                'input' => '<input type="{{type}}" name="{{name}}"{{attrs}} class="form-control col-sm-6"/>',
                'inputContainer' => '<div class="form-group row {{type}}{{required}}">{{content}}</div>',
                'inputContainerError' => '<div class="form-group row {{type}}{{required}} error">{{content}}{{error}}</div>',
                'label' => '<label{{attrs}} class="col-sm-2 offset-sm-2 col-form-label">Apellido Paterno</label>',
            ]]) ?>

        <?= $this->Form->control('a_materno', [
            'templates' => [
                'error' => '<div class="invalid-feedback offset-sm-4">{{content}}</div>',
                'input' => '<input type="{{type}}" name="{{name}}"{{attrs}} class="form-control col-sm-6"/>',
                'inputContainer' => '<div class="form-group row {{type}}{{required}}">{{content}}</div>',
                'inputContainerError' => '<div class="form-group row {{type}}{{required}} error">{{content}}{{error}}</div>',
                'label' => '<label{{attrs}} class="col-sm-2 offset-sm-2 col-form-label">Apellido Materno</label>',
            ]]) ?>

        <div class="form-check form-group row">
            <div class="col-sm-6 offset-sm-2 custom-control custom-checkbox">
                <?= $this->Form->checkbox('habilitado', ['class' => 'custom-control-input', 'id' => 'habilitado']); ?>
                <?= $this->Form->label('habilitado', 'Habilitado', ['class' => 'custom-control-label'] ); ?>
            </div>
        </div>

        <div class="form-check form-group row">
            <div class="col-sm-6 offset-sm-2 custom-control custom-checkbox">
                <?= $this->Form->checkbox('administrador', ['class' => 'custom-control-input', 'id' => 'administrador']); ?>
                <?= $this->Form->label('administrador', 'Administrador', ['class' => 'custom-control-label'] ); ?>
            </div>
        </div>

        <?= $this->Form->input('photo', [
            'type' => 'file',
            'class' => 'custom-file-input',
            'lang' => 'es',
            'templates' => [
                'formGroup' => '<div class="col-sm-8 offset-sm-2">{{input}}{{label}}</div>',
                'error' => '<div class="offset-sm-2">{{content}}</div>',
                'inputContainer' => '<div class="custom-file {{type}}{{required}}">{{content}}</div>',
                'inputContainerError' => '<div class="custom-file {{type}}{{required}} error invalid-feedback">{{content}}{{error}}</div>',
                'label' => '<label{{attrs}} class="custom-file-label">Foto de usuario</label>',
            ]
        ]); ?>

    </fieldset>
    <div class="row" style="margin-top: 20px">
        <div class="btn-group btn-group-lg offset-sm-4 col-sm-2">
            <?= $this->Form->button(__('Enviar'), ['class' => 'btn btn-primary btn-block']) ?>
        </div>
        <div class="btn-group btn-group-lg col-sm-2">
            <?= $this->Html->link(__('Cancelar'), ['action' => 'index'],['class' => 'btn btn-danger btn-block']) ?>
        </div>
    </div>

    <?= $this->Form->end() ?>
</div>
