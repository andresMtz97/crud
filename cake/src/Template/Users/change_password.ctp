<?php
$this->loadHelper('Form', [
   'errorClass' => 'form-control col-sm-4 offset-sm-4 is-invalid',
]);

 ?>
<nav class="navbar navbar-expand-lg navbar-dark bg-primary fixed-top">
        <?= $this->Html->link(__('Usuarios'), ['action' => 'index'], ['class' => 'navbar-brand h1 mb-0']) ?>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarToggler" aria-controls="navbarToggler" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarToggler">
        <ul class="navbar-nav ml-auto mt-2 mt-lg-0">
            <li class="nav-item active my-2 my-sm-0">
                <?= $this->Html->link(__('Cerrar Sesión'), ['action' => 'logout'], ['class' => 'nav-link btn btn-danger btn-sm ']) ?>
            </li>
        </ul>
    </div>
</nav>

<div class="container">
    <?= $this->Form->create($user) ?>
    <fieldset>
        <legend style="text-align: center"><?= __('Por favor ingrese su contraseña actual y la nueva contraseña') ?></legend>
        <?= $this->Form->control('oldPassword', [
            'type' => 'password',
            'templates' => [
                'error' => '<div class="invalid-feedback offset-sm-4 {{type}}">{{content}}</div>',
                'input' => '<input type={{type}} name="{{name}}"{{attrs}} class="form-control col-sm-4 offset-sm-4" placeholder="Contraseña actual"/>',
                'inputContainer' => '<div class="form-group row {{type}}{{required}}">{{content}}</div>',
                'inputContainerError' => '<div class="form-group row">{{content}}{{error}}</div>',
                'label' => '<label{{attrs}} class="col-sm-2 offset-sm-4 col-form-label" hidden>{{text}}</label>',
            ]]) ?>

        <?= $this->Form->control('newPassword', [
            'type' => 'password',
            'templates' => [
                'error' => '<div class="invalid-feedback offset-sm-4 col-sm-4 {{type}}">{{content}}</div>',
                'input' => '<input type={{type}} name="{{name}}"{{attrs}} class="form-control col-sm-4 offset-sm-4" placeholder="Nueva Contraseña"/>',
                'inputContainer' => '<div class="form-group row {{type}}{{required}}">{{content}}</div>',
                'inputContainerError' => '<div class="form-group row">{{content}}{{error}}</div>',
                'label' => '<label{{attrs}} class="col-sm-2 offset-sm-4 col-form-label" hidden>{{text}}</label>',
            ]]) ?>

        <?= $this->Form->control('newPassword2', [
            'type' => 'password',
            'templates' => [
                'error' => '<div class="invalid-feedback offset-sm-4 col-sm-4 {{type}}">{{content}}</div>',
                'input' => '<input type={{type}} name="{{name}}"{{attrs}} class="form-control col-sm-4 offset-sm-4" placeholder="Repite Nueva Contraseña"/>',
                'inputContainer' => '<div class="form-group row {{type}}{{required}}">{{content}}</div>',
                'inputContainerError' => '<div class="form-group row">{{content}}{{error}}</div>',
                'label' => '<label{{attrs}} class="col-sm-2 offset-sm-4 col-form-label" hidden>{{text}}</label>',
            ]]) ?>
    </fieldset>

    <div class="row">
        <div class="btn-group btn-group-lg offset-sm-4 col-sm-2">
            <?= $this->Form->button('Enviar', ['class' => 'btn btn-info btn-block ']) ?>
        </div>
        <div class="btn-group btn-group-lg col-sm-2">
            <?= $this->Html->link(__('Cancelar'), ['action' => 'index'], ['class' => 'btn btn-danger btn-block']) ?>
        </div>
    </div>

    <?= $this->Form->end() ?>
</div>
