<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN">
<html>
<head>
    <title><?= $this->fetch('title') ?></title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

    <style>
		.ReadMsgBody {width: 100%; background-color: #ffffff;}
		.ExternalClass {width: 100%; background-color: #ffffff;}

				/* Windows Phone Viewport Fix */
		@-ms-viewport {
		    width: device-width;
		}
	</style>
</head>
<body>
    <?= $this->fetch('content') ?>
</body>
</html>
