<?php
namespace App\Mailer;

use Cake\Mailer\Mailer;

/**
 * Users mailer.
 */
class UsersMailer extends Mailer
{

    /**
     * Mailer's name.
     *
     * @var string
     */
    public static $name = 'Users';

    public function sendPassword($user, $password){
        $this->to($user->email)
            ->profile('becario')
            ->emailFormaT('html')
            ->template('password')
            ->layout('users')
            ->viewVars(['email' => $user->email, 'password' => $password])
            ->subject(sprintf('Binevenido %s', $user->nombre . ' ' . $user->a_paterno . ' ' . $user->a_materno));
    }

    public function sendResetEmail($url, $user) {
        $this->to($user->email)
        ->profile('becario')
        ->emailFormat('html')
        ->template('resetpw')
        ->viewVars(['url' => $url, 'username' => $user->email])
        ->subject('Reset your password');
    }
}
