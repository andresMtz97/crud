<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Auth\DefaultPasswordHasher;
use Cake\Utility\Text;
use Cake\Event\Event;
use Cake\Utility\Security;

/**
 * Users Model
 *
 * @method \App\Model\Entity\User get($primaryKey, $options = [])
 * @method \App\Model\Entity\User newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\User[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\User|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\User|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\User patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\User[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\User findOrCreate($search, callable $callback = null, $options = [])
 */
class UsersTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('users');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Proffer.Proffer', [
        	'photo' => [	// The name of your upload field
        		'root' => WWW_ROOT . 'files', // Customise the root upload folder here, or omit to use the default
        		'dir' => 'photo_dir',	// The name of the field to store the folder
        		'thumbnailSizes' => [ // Declare your thumbnails
        			// 'square' => [	// Define the prefix of your thumbnail
        			// 	'w' => 200,	// Width
        			// 	'h' => 200,	// Height
                    //     'crop' => true,
        			// 	'jpeg_quality'	=> 100
        			// ],
        			// 'portrait' => [		// Define a second thumbnail
        			// 	'w' => 100,
        			// 	'h' => 300
        			// ],
        		],
        		'thumbnailMethod' => 'gd'	// Options are Imagick or Gd
        	]
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->email('email', null, 'El correo proporcionado no es válido.')
            ->requirePresence('email', 'create', 'Este campo es obligatorio.')
            ->allowEmptyString('email', false, 'Este campo es obligatorio.');

        $validator
            ->scalar('nombre')
            ->maxLength('nombre', 50)
            ->requirePresence('nombre', 'create', 'Este campo es obligatorio.')
            ->allowEmptyString('nombre', false, 'Este campo es obligatorio.')
            ->add('nombre', 'validFormat',
                ['rule' => ['custom', '/^[a-zA-ZáéíóúÁÉÍÓÚñÑ\s]+$/'],
                'message' => 'El nombre solo puede llevar letras.']);

        $validator
            ->scalar('a_paterno')
            ->maxLength('a_paterno', 50)
            ->requirePresence('a_paterno', 'create', 'Este campo es obligatorio.')
            ->allowEmptyString('a_paterno', false, 'Este campo es obligatorio.')
            ->add('a_paterno', 'validFormat',
                ['rule' => ['custom', '/^[a-zA-ZáéíóúÁÉÍÓÚñÑ\s]+$/'],
                'message' => 'El Apellido solo puede llevar letras.']);

        $validator
            ->scalar('a_materno')
            ->maxLength('a_materno', 50)
            ->allowEmptyString('a_materno')
            ->add('a_materno', 'validFormat',
                ['rule' => ['custom', '/^[a-zA-ZáéíóúÁÉÍÓÚñÑ\s]*$/'],
                'message' => 'El apellido solo puede llevar letras.']);

        $validator
            ->boolean('habilitado')
            ->requirePresence('habilitado', 'create', 'Este campo es obligatorio.')
            ->allowEmptyString('habilitado', false);

        $validator
            ->boolean('administrador')
            ->requirePresence('administrador', 'create', 'Este campo es obligatorio.')
            ->allowEmptyString('administrador', false);

        $validator
            ->provider('proffer', 'Proffer\Model\Validation\ProfferRules')
            ->add('photo', 'extension', [
                'rule' => ['extension', ['jpg', 'gif', 'png']],
                'message' => 'La imagen debe ser jpg, gif o png.'])
            ->add('photo', 'fileSize', [
                'rule' => ['fileSize', '<=', '1MB'],
                'message' => 'La imagen no debe exceder 1MB.'])
            ->requirePresence('photo', 'create', 'Este campo es obligatorio.')
            ->allowEmptyFile('photo');

        return $validator;
    }

    public function validationChangePassword(Validator $validator)
    {
        $validator
            ->scalar('newPassword')
            ->requirePresence('newPassword')
            ->add('newPassword', 'validFormat',
                ['rule' => ['custom', '/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-.]).{8,20}$/'],
                'message' => 'La contraseña debe contener entre 8 y 20 caracteres y al menos una minúscula, una mayuscula y un caracter especial (#, ?, !, @, $, %, ^, &, *, -, .)']);
        $validator
            ->requirePresence('oldPassword', true, 'Este campo es obligatorio.');

        $validator
            ->requirePresence('newPassword2', true, 'Este campo es obligatorio.');

        return $validator;
    }

    public function validationResetPassword(Validator $validator)
    {
        $validator
            ->scalar('password')
            ->requirePresence('password')
            ->add('password', 'validFormat',
                ['rule' => ['custom', '/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-.]).{8,20}$/'],
                'message' => 'La contraseña debe contener entre 8 y 20 caracteres y al menos una minúscula, una mayuscula y un caracter especial (#, ?, !, @, $, %, ^, &, *, -, .)']);
        $validator
            ->requirePresence('confirm_password', true, 'Este campo es obligatorio.');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['email'], __('El email ingresado ya está registrado')));

        return $rules;
    }

    public function beforeSave(Event $event)
    {
        $entity = $event->getData('entity');

        if ($entity->isNew()) {
            $hasher = new DefaultPasswordHasher();
            // Generate an API 'token'
            $entity->api_key_plain = Security::hash(Security::randomBytes(32), 'sha256', false);
            // Bcrypt the token so BasicAuthenticate can check
            // it during login.
            $entity->api_key = $hasher->hash($entity->api_key_plain);
        }
        return true;
    }

    public function findAuth(\Cake\ORM\Query $query, array $options){
        $query
            ->select(['id', 'nombre', 'a_paterno', 'a_materno', 'email', 'password', 'administrador'])
            ->where(['Users.habilitado' => true]);
        return $query;
    }
}
