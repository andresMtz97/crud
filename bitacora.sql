create table bitacora (
	id serial primary key,
	id_usuario integer not null,
	usuario varchar(50) not null,
	accion varchar(25) not null,
	fecha_hora timestamp not null,
	constraint fk_bit_usu foreign key (id_usuario)
	references users (id)
);